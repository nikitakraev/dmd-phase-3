package com.project.model.entities;

public interface Writable {
	byte[] serialize();
	void build(byte[] array);
	int length();
}
