package test;

import com.project.CommandProcessor;
import com.project.StorageManager;
import com.project.model.Pair;
import com.project.model.Table;
import com.project.model.tuple.Tuple;
import com.project.model.tuple.TupleBuilder;
import com.project.model.entities.SimpleEntity;
import com.project.model.tree.BTree;

import org.testng.annotations.Test;

import java.io.IOException;
import java.util.List;

/**
 * @author darkwizard
 */

public class StorageManagerTest {

	@Test
	public void testWriting() {
		// test for writing some kind of data to database
		StorageManager manager = new StorageManager();
		Tuple t = new Tuple("publication", "id", "name");
		Tuple t2 = new Tuple("Hello, world!", "3", "Qiang Qu");
		manager.write(t);
		manager.write(t2);

		manager.close();

		assert t.serialize().length > 0;
	}

	public String generateString(int length) {
		String res = "";
		for (int i = 0; i < length; ++i) {
			res = res + (char) (65 + (int)(Math.random() * 26));
		}

		return res;
	}

	@Test
	public void testDeletion() {
		StorageManager manager = new StorageManager();
		Tuple t = new Tuple();
		for (int i = 0; i < 5; ++i) {
			t = new Tuple(this.generateString(10), i + "", this.generateString(25));
			//System.out.println(a);
			manager.write(t);
		}
		manager.close();
		assert t.getKeys().size() == 3;
	}

	public Table getTable() {
		Table t = new Table("Authors", "publication", "id", "name");
		StorageManager manager = new StorageManager();
		for (int i = 0; i < 25; ++i) {
			Tuple tup = t.buildTuple(this.generateString(10), i + "", this.generateString(25));

			int offset = manager.write(tup);
			int len = tup.serialize().length;
			t.insertTuple(tup, len, offset);
		}

		manager.close();
		return t;
	}

	public CommandProcessor getCP() {
		Table t = getTable();
		CommandProcessor cp = new CommandProcessor();
		cp.testAddTables(t);

		return cp;
	}

	@Test
	public void testTable() {
		Table t = getTable();
		assert t.getTree("id").size() == 25;
	}

	@Test
	public void testCP() {
		Table t = getTable();
		CommandProcessor cp = new CommandProcessor();
		cp.testAddTables(t);

		assert cp.init().scan(t.getTableName()).list().size() == 25;
		cp.close();
	}

	@Test
	public void testTree() {
		BTree tree = new BTree();

		StorageManager manager = new StorageManager();
		for (int i = 0; i < 25; ++i) {
			TupleBuilder tb = new TupleBuilder().table("Authors").init("id", "name", "publication");
			Tuple t = tb.create("1", "Vasya", "Hello, world!");
			int offset = manager.write(t);
			tree.insert(new SimpleEntity(t, "id"), new Pair(offset, t.serialize().length));
		}

		manager.close();

		tree.size();
		assert tree.size() == 25;
		tree.serialize();
	}

	@Test
	public void testTuple() {
		TupleBuilder tb = new TupleBuilder().table("Authors").init("name", "id", "publication");
		Tuple t = tb.create(this.generateString(10), "1", this.generateString(25));
		assert t.getKeys().size() == 3;
	}

	@Test
	public void testTupleFiltering() {
		CommandProcessor cp = getCP();
		assert cp.scan("Authors").filter("id = 21").list().size() == 1;
		cp.close();
	}

	@Test
	public void testTableWriting() {
		Table t = new Table("Authors", "id", "name", "value");
		CommandProcessor cp = new CommandProcessor();
		cp.testAddTables(t);
		cp.close();

		cp = new CommandProcessor();
		assert cp.scan("Authors").list().size() == 1;
		cp.close();
	}

	@Test
	public void testAdvancedTupleFiltering() {
		StorageManager sm = new StorageManager();

		Table t = new Table("Authors", "id", "name", "publication");

		Tuple tuple = t.buildTuple("1", "Vasya", "Hello, world!");
		int offset = sm.write(tuple);
		t.insertTuple(tuple, tuple.serialize().length, offset);
		sm.close();

		CommandProcessor cp = new CommandProcessor();
		cp.testAddTables(t);
		List<Tuple> tupleList = cp.init().scan("Authors").filter("name = Vasya").list();
		assert tupleList.size() == 1;
		assert tupleList.get(0).get("name").equals("Vasya");

		cp.close();
	}

	@Test
	public void testTupleProjection() {
		CommandProcessor cp = getCP();
		List<Tuple> res = cp.init().scan("Authors").project("id").list();
		assert res.get(0).getKeys().size() == 1;
		cp.close();
	}

	@Test
	public void testTupleJoinCoalesce() {
		Table pub = new Table("Publications", "id", "name", "numOfPages");
		Table auth = new Table("Authors", "id", "name", "pubid");

		StorageManager sm = new StorageManager();
		Tuple pt = pub.buildTuple("1", "Vasya's life", "650");
		int offset = sm.write(pt);
		pub.insertTuple(pt, pt.length(), offset);

		Tuple at = auth.buildTuple("1", "Vasya Petrov", "2");
		offset = sm.write(at);
		auth.insertTuple(at, at.length(), offset);

		at = auth.buildTuple("1", "Vasya Ivanov", "1");
		offset = sm.write(at);
		auth.insertTuple(at, at.length(), offset);

		sm.close();

		CommandProcessor cp = new CommandProcessor();
		cp.testAddTables(pub, auth);

		List<Tuple> allPubs = cp.init().scan(pub.getTableName()).list();

		List<Tuple> merged = cp.init()
				.scan(auth.getTableName())
				.join(allPubs, "Authors.pubid", "Publications.id", "LEFT OUTER")
				.coalesce("EMPTY")
				.list();
		assert merged.size() == 2;
		cp.close();
	}

	@Test
	public void testGroupByOffsetLimit() {
		Table auth = new Table("Authors", "id", "name", "pubid");
		StorageManager sm = new StorageManager();

		Tuple at = auth.buildTuple("1", "Vasya", "2");
		int offset = sm.write(at);
		auth.insertTuple(at, at.length(), offset);

		at = auth.buildTuple("1", "Vasya", "1");
		offset = sm.write(at);
		auth.insertTuple(at, at.length(), offset);

		sm.close();

		CommandProcessor cp = new CommandProcessor();
		cp.testAddTables(auth);

		List<Tuple> res = cp.init()
				.scan("Authors")
				.limit(1)
				.list();
		assert res.size() == 1;
		cp.close();
	}

	@Test
	public void testCount() {
		Table auth = new Table("Authors", "id", "name", "pubid");
		StorageManager sm = new StorageManager();

		Tuple at = auth.buildTuple("1", "Vasya", "2");
		int offset = sm.write(at);
		auth.insertTuple(at, at.length(), offset);

		at = auth.buildTuple("1", "Vasya", "1");
		offset = sm.write(at);
		auth.insertTuple(at, at.length(), offset);

		sm.close();

		CommandProcessor cp = new CommandProcessor();
		cp.testAddTables(auth);

		List<Tuple> res = cp.init()
				.scan("Authors")
				.count("name")
				.list();

		assert res.size() == 2;
		cp.close();
	}
}