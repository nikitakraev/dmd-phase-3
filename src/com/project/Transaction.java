package com.project;


import com.project.model.entities.Writable;

public class Transaction {

	public Transaction() {
		// opening .transaction
	}

	public void addToTransaction(Writable data) {
		// writing binary data to .transaction
	}

	public static void commit() {
		// merge .transaction file into .db
	}
}
